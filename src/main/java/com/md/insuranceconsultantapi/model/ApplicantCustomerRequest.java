package com.md.insuranceconsultantapi.model;

import com.md.insuranceconsultantapi.enums.Gender;
import com.md.insuranceconsultantapi.enums.InsuranceGrade;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ApplicantCustomerRequest {
    private String name;
    private LocalDate reservationDate;
    private String birthDate;
    private String phoneNumber;
    private String address;
    @Enumerated(value = EnumType.STRING)
    private Gender gender;
    @Enumerated(value = EnumType.STRING)
    private InsuranceGrade insuranceGrade;
    private String memo;
}
