package com.md.insuranceconsultantapi.model;

import com.md.insuranceconsultantapi.enums.Gender;
import com.md.insuranceconsultantapi.enums.InsuranceGrade;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ApplicantCustomerResponse {
    private Long id;
    private String name;
    private LocalDate requestDate;
    private LocalDate reservationDate;
    private String birthDate;
    private String phoneNumber;
    private String genderName;
    private String insuranceGradeName;
    private String isJoin;
    private String memo;
}
