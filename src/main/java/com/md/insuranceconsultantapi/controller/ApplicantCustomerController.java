package com.md.insuranceconsultantapi.controller;

import com.md.insuranceconsultantapi.entity.ApplicantCustomer;
import com.md.insuranceconsultantapi.model.ApplicantCustomerItem;
import com.md.insuranceconsultantapi.model.ApplicantCustomerRequest;
import com.md.insuranceconsultantapi.model.ApplicantCustomerResponse;
import com.md.insuranceconsultantapi.service.ApplicantCustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/applicant-customer")
public class ApplicantCustomerController {
    private final ApplicantCustomerService applicantCustomerService;

    @PostMapping("/people")
    public String setApplicantCustomer(@RequestBody ApplicantCustomerRequest request) {
        applicantCustomerService.setApplicantCustomer(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<ApplicantCustomerItem> getApplicantCustomers() {
        return applicantCustomerService.getApplicantCustomer();
    }

    @GetMapping("/detail/{id}")
    public ApplicantCustomerResponse getApplicantCustomer(@PathVariable long id) {
        return applicantCustomerService.getApplicantCustomer(id);
    }
}
