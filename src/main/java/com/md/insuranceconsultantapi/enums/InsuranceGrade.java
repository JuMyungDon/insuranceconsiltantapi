package com.md.insuranceconsultantapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum InsuranceGrade {
    RAITING1("1등급", true),
    RAITING2("2등급", true),
    RAITING3("3등급", true),
    RAITING4("4등급", false);

    private final String gradeName;
    private final Boolean isJoin;
}
