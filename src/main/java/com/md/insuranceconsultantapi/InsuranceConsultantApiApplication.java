package com.md.insuranceconsultantapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsuranceConsultantApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(InsuranceConsultantApiApplication.class, args);
    }

}
