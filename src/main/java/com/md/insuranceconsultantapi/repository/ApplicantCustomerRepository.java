package com.md.insuranceconsultantapi.repository;

import com.md.insuranceconsultantapi.entity.ApplicantCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicantCustomerRepository extends JpaRepository<ApplicantCustomer, Long> {
}
