package com.md.insuranceconsultantapi.service;

import com.md.insuranceconsultantapi.entity.ApplicantCustomer;
import com.md.insuranceconsultantapi.model.ApplicantCustomerItem;
import com.md.insuranceconsultantapi.model.ApplicantCustomerRequest;
import com.md.insuranceconsultantapi.model.ApplicantCustomerResponse;
import com.md.insuranceconsultantapi.repository.ApplicantCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ApplicantCustomerService {
    private final ApplicantCustomerRepository applicantCustomerRepository;

    public void setApplicantCustomer(ApplicantCustomerRequest request) {
        ApplicantCustomer addData = new ApplicantCustomer();
        addData.setName(request.getName());
        addData.setRequestDate(LocalDate.now());
        addData.setReservationDate(request.getReservationDate());
        addData.setBirthDate(request.getBirthDate());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setAddress(request.getAddress());
        addData.setGender(request.getGender());
        addData.setInsuranceGrade(request.getInsuranceGrade());
        addData.setMemo(request.getMemo());

        applicantCustomerRepository.save(addData);
    }

    public List<ApplicantCustomerItem> getApplicantCustomer() {
        List<ApplicantCustomer> originList = applicantCustomerRepository.findAll();

        List<ApplicantCustomerItem> result = new LinkedList<>();

        for (ApplicantCustomer applicantCustomer : originList) {
            ApplicantCustomerItem addItem = new ApplicantCustomerItem();
            addItem.setId(applicantCustomer.getId());
            addItem.setName(applicantCustomer.getName());
            addItem.setRequestDate(applicantCustomer.getRequestDate());
            addItem.setReservationDate(applicantCustomer.getReservationDate());
            addItem.setBirthDate(applicantCustomer.getBirthDate());
            addItem.setPhoneNumber(applicantCustomer.getPhoneNumber());
            addItem.setAddress(applicantCustomer.getAddress());
            addItem.setGenderName(applicantCustomer.getGender().getGenderName());
            addItem.setGradeName(applicantCustomer.getInsuranceGrade().getGradeName());
            addItem.setIsJoin(applicantCustomer.getInsuranceGrade().getIsJoin() ? "예" : "아니오");

            result.add(addItem);
        }

        return result;
    }

    public ApplicantCustomerResponse getApplicantCustomer(long id) {
        ApplicantCustomer originData = applicantCustomerRepository.findById(id).orElseThrow();

        ApplicantCustomerResponse response = new ApplicantCustomerResponse();
        response.setId(originData.getId());
        response.setName(originData.getName());
        response.setRequestDate(originData.getRequestDate());
        response.setReservationDate(originData.getReservationDate());
        response.setBirthDate(originData.getBirthDate());
        response.setPhoneNumber(originData.getPhoneNumber());
        response.setGenderName(originData.getGender().getGenderName());
        response.setInsuranceGradeName(originData.getInsuranceGrade().getGradeName());
        response.setIsJoin(originData.getInsuranceGrade().getIsJoin() ? "예" : "아니오");
        response.setMemo(originData.getMemo());

        return response;
    }
}
